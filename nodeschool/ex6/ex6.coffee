ls_module = require './ls_module'

path_to_dir = process.argv[2]

ext = process.argv[3]

ls_module path_to_dir, ext, (err, files) ->
	if (err)
		return console.error("Error: ", err)
	files.forEach (item) ->
		console.log item
