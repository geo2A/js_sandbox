#function lists files with extension 'ext' in directory 'dirName'
ls = (dirName, ext, callback) -> 
	fs = require 'fs'

	fs.readdir dirName, (err, list) ->
		if err 
			return callback err
		else
			re = new RegExp("^.*\\." + ext + "$") #regexp for files with extension 'ext'
			result = list.filter (item) ->
				re.test item, dirName
			return callback null, result

module.exports = ls