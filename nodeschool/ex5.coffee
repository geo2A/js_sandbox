fs = require "fs"

pathToFile = process.argv[2]
ext = process.argv[3]

fs.readdir pathToFile, (err, list) ->
	if err 
		throw err
	else
		re = new RegExp("^.*\\." + ext + "$")
		console.log list.filter (item) ->
			re.test item
