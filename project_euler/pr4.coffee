isPal = (n) ->
	digits = new String n #making an array from number
	len = digits.length
	comps = [0..len/2].map (i) -> digits[i] == digits[len-i-1]
	comps.reduce (a,b) -> a && b

vals = []
vals.push(x * y) for x in [100..999] for y in [100..999]
biggestPalindrome = (vals.filter (n) -> isPal(n)).reduce (a,b) -> Math.max(a,b)
console.log biggestPalindrome
