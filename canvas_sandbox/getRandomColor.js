/*** gets color by point coordinates ***/
function getRandomColor(x, y) {
    var letters = 'ABCDEF'.split('');
    var color = '#00';
    for (var i = 2; i < 4; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    color += "FF";
    return color;
}