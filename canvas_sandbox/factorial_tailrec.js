function fact_recur(n, fact_accum){
    if (n === 0) {
        return fact_accum;
    } else {
        return fact_recur(n - 1, fact_accum * n);
    }
}

console.log(fact_recur(5,1));