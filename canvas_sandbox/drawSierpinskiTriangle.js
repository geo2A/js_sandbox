function drawEquilateralTriangle(x, y, side_length){
   canvas.strokeStyle=getRandomColor();
   var h = side_length * Math.sqrt(3) * 0.5;
   canvas.beginPath();
   canvas.moveTo(x, y);
   canvas.lineTo(x + side_length, y);
   canvas.lineTo(x + 0.5 * side_length, y - h);
   canvas.lineTo(x, y);
   canvas.stroke();
   canvas.closePath(); 
}

function drawSierpinskiTriangle(x, y, side_length, depth){
    if (depth === 1){
        return;
    } else {
        var h = side_length * Math.sqrt(3) * 0.5;
        drawEquilateralTriangle(x, y, side_length);
        drawSierpinskiTriangle(x, y, 0.5 * side_length, depth - 1);
        drawSierpinskiTriangle(x + 0.5 * side_length, y, 0.5 * side_length, depth - 1);
        drawSierpinskiTriangle(x + 0.25 * side_length, y - 0.5 * h, 0.5 * side_length, depth - 1);
    }
}

/**** geting canvas element and start drawing ****/
var sandbox_canvas = document.getElementById("sandbox_canvas"),
    canvas     = sandbox_canvas.getContext('2d');
    canvas.strokeStyle="#2EB8E6";
    canvas.fillStyle="#0A1F33";
    canvas.fillRect(0, 0, sandbox_canvas.width, sandbox_canvas.height);
    drawSierpinskiTriangle(75, 600, 650, 9);