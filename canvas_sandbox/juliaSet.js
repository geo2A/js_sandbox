function point(pos, canvas){
    canvas.fillStyle=getRandomColor();   
    canvas.fillRect(pos[0], pos[1], 1, 1);
}

function conversion(x, y, width, R){
    var m = R / width;
    var x1 = m * (2 * x - width);
    var y2 = m * (width - 2 * y);
    return [x1, y2];
}

function f(z, c){
    return [z[0]*z[0] - z[1] * z[1] + c[0], 2 * z[0] * z[1] + c[1]];
}

function abs(z){
    return Math.sqrt(z[0]*z[0] + z[1]*z[1]);
}

function init(){
    var length = 800,
        width = 800,
        c = [0, 1],
        maxIterate = 12,
        R = (1 + Math.sqrt(1+4*abs(c))) / 2,
        z;

    var canvas = document.getElementById('sandbox_canvas').getContext("2d");
    canvas.fillStyle="#0A1F33";
    canvas.fillRect(0, 0, sandbox_canvas.width, sandbox_canvas.height);

    var flag;
    for (var x = 0; x < width; x++){
        for (var y = 0; y < length; y++){
            flag = true;
            z = conversion(x, y, width, R);
            for (var i = 0; i < maxIterate; i++){
                z = f(z, c);
                if (abs(z) > R){
                    flag = false;
                    break;
                }
            }
            if (flag) point([x, y], canvas);
        }
    }
}
init();