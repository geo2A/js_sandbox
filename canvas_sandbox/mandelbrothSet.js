/**
 ** draws point of given color
 ** pos = [x,y]
 */
function point(pos, canvas, color){
    canvas.fillStyle=color;   
    canvas.fillRect(pos[0], pos[1], 1, 1);
}

function conversion(x, y, width, R){
    var m = R / width;
    var x1 = m * (2 * x - width);
    var y2 = m * (width - 2 * y);
    return [x1, y2];
}

function f(z, c){
    // return [z[0]*z[0] - z[1] * z[1] + c[0], 2 * z[0] * z[1] + c[1]];
    return [z[0] * z[0] - z[1] * z[1] + c[0], 2 * z[0] * z[1] + c[1]];
}

function abs(z){
    return Math.sqrt(z[0]*z[0] + z[1]*z[1]);
}

function init(){
    var length = 768,
        width = 768,
        c = [0, 1],
        maxIterate = 15,
        R = (1 + Math.sqrt(1+4*abs(c))) / 2,
        z;

    var canvas = document.getElementById('sandbox_canvas').getContext("2d");
    canvas.fillStyle="#0A1F33";
    // canvas.fillRect(0, 0, sandbox_canvas.width, sandbox_canvas.height);

    var flag;
    for (var x = 0; x < width; x++){
        for (var y = 0; y < length; y++){
            flag = true;
            z = conversion(x, y, width, R);
            for (var i = 0; i < maxIterate; i++){
                z = f(z, c);
                if (abs(z) > R){
                    flag = false;
                    break;
                }
            }
            if (flag) point([x, y], 
                            canvas, 
                            "#000000");
        }
    }
}
/**
 ** gets color by point coordinates 
 ** pos = [x,y]
 */
// function getColor(pos) {
//     /*var letters = 'ABCDEF'.split('');
    
//     for (var i = 2; i < 4; i++ ) {
//         color += letters[Math.floor(Math.random() * 16)];
//     }
//     color += "FF";*/
//     var color = '#';
//     var pos_abs = abs(pos);
//     if ((pos_abs >= 0) && (pos_abs <= 0.1)) {
//         color += "00FFFF";
//     }
//     if ((pos_abs > 0.1) && (pos_abs <= 0.2)){
//         color += "00E6E6";
//     }
//     if ((pos_abs > 0.2) && (pos_abs <= 0.3)){ 
//         color += "00CCCC";
//     }
//     if ((pos_abs > 0.3) && (pos_abs <= 0.4)){ 
//         color += "00B2B2";
//     }
//     if ((pos_abs > 0.4) && (pos_abs <= 0.5)){ 
//         color += "009999";
//     }
//     if ((pos_abs > 0.5) && (pos_abs <= 0.6)){ 
//         color += "008080";
//     }
//     if ((pos_abs > 0.6) && (pos_abs <= 0.7)){ 
//         color += "006666";
//     }
//     if ((pos_abs > 0.7) && (pos_abs <= 0.8)){ 
//         color += "004C4C";
//     }
//     if (pos_abs > 0.8){ 
//         color += "003333";
//     }
//     return color;
// }
init();